﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NRE_Portal_TP_Agile.Controllers
{
    public class Node
    {
        public Node(double measure, int year)
        {
            this.measure = measure;
            this.year = year;
        }

        public double measure { get; set; }

        public int year { get; set; }
    }
}
