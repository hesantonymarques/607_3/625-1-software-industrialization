﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;

namespace NRE_Portal_TP_Agile.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PhotovoltaicController : ControllerBase
    {

        [HttpGet]
        public IEnumerable<Node> Get()
        {

            List<Node> DataPoints1 = new List<Node>();

            using (var reader = new StreamReader(@"./Controllers/photovoltaicData.csv"))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(';');
               
                    DataPoints1.Add(new Node(Convert.ToDouble(values[1].Replace(",", ".").Trim()), Int16.Parse(values[0])));
                }

            }
            return DataPoints1.ToArray();
        }
    }
}
