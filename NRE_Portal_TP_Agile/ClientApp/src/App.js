import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import { Layout } from './components/Layout';
import { Home } from './components/Home';
import { NER } from './components/NER';
import { PhotovoltaicDetails } from './components/PhotovoltaicDetails';
import { BiogasDetails } from './components/BiogasDetails';
import { WindturbineDetails } from './components/WindturbineDetails';
import './custom.css'

export default class App extends Component {
  static displayName = App.name;

  render () {
    return (
      <Layout>
        <Route exact path='/' component={Home} />
        <Route path='/NER' component={NER} />
        <Route path='/Biogas' component={BiogasDetails} />
        <Route path='/Windturbine' component={WindturbineDetails} />
        <Route path='/Photovoltaic' component={PhotovoltaicDetails} />
      </Layout>
    );
  }
}
