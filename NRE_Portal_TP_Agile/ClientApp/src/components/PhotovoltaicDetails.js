﻿import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import CanvasJSReact from '../lib/canvasjs.react';
var CanvasJSChart = CanvasJSReact.CanvasJSChart;

export class PhotovoltaicDetails extends Component {
	static displayName = PhotovoltaicDetails.name;

	constructor(props) {
		super(props);
		this.state = { photovoltaicData: [], loading: true };
	}

	componentDidMount() {
		this.getPhotovoltaicData();
	}

	static renderChart(data) {
		var displayedData = [];

		const options = {
			animationEnabled: true,
			title: {
				text: "Production of photovoltaic electricity since 1990 (KWh)"
			},
			axisX: {
				title: "Year",

			},

			axisY: {
				title: "Photovoltaic electricity in KWh",
				suffix: " KWh"
			},
			legend: {
				cursor: "pointer",
				fontSize: 16,
			},
			toolTip: {
				shared: true
			},
			data: [
				{
					name: "Renewable electricity production",
					type: "spline",
					yValueFormatString: "#0.## KWh",
					showInLegend: true,
					dataPoints: displayedData,
				},
			]
		}

		for (var i = 0; i <= data.length; i++) {
			try {
				displayedData.push({ x: data[i].year, y: data[i].measure * 1000 });
			} catch {
				//Data will be generated automatically
			}
		}

		options.data[0].dataPoints = displayedData;

		return <CanvasJSChart options={options} />;

	}

	render() {
		let contents = this.state.loading
			? <p><em>Loading...</em></p>
			: PhotovoltaicDetails.renderChart(this.state.photovoltaicData);

		return (
			<div>
				<h1>Photovoltaic</h1>

				<p><Link to="/">Home</Link> &gt; <Link to="/NER">NER</Link> &gt; <Link to="/Photovoltaic">Photovoltaic</Link></p>
				{contents}

			</div>
		);
	}

	async getPhotovoltaicData() {
		const response = await fetch('photovoltaic');
		const data = await response.json();
		this.setState({ photovoltaicData: data, loading: false });
	}
}