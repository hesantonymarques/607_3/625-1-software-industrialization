import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import CanvasJSReact from '../lib/canvasjs.react';
var CanvasJSChart = CanvasJSReact.CanvasJSChart;

export class WindturbineDetails extends Component {
    static displayName = WindturbineDetails.name;

    constructor(props) {
		super(props);
		this.state = { windturbineData: [], loading: true };
	}

	componentDidMount() {
		this.getWindturbineData();
	}

	static renderChart(data) {
		var data1 = [];

		const options = {
			animationEnabled: true,
			title: {
				text: "Development of renewable windturbine energies since 1990 (KWh)"
			},
			axisX: {
				title: "Year",

            },
			
			axisY: {
				title: "Renewable Wind in KWh",
				suffix: " KWh"
			},
			legend: {
				cursor: "pointer",
				fontSize: 16,
			},
			toolTip: {
				shared: true
			},
			data: [
				{
					name: "Renewable electricity production(excluding hydropower)",
					type: "spline",
					yValueFormatString: "#0.## KWh",
					showInLegend: true,
					dataPoints: data1,
				},
			]
		}

		for (var i = 0; i <= data.length; i++) {
			try {
				data1.push({ x: data[i].year, y: data[i].measure * 1000 });
			} catch {
				//Data will be generated automatically
            }
		}

		options.data[0].dataPoints = data1;

        return <CanvasJSChart options={options} />;

	}

	render() {
		let contents = this.state.loading
			? <p><em>Loading...</em></p>
			: WindturbineDetails.renderChart(this.state.windturbineData);
      
        return (
            <div>
                <h1>Windturbine</h1>

				<p><Link to="/">Home</Link> &gt; <Link to="/NER">NER</Link> &gt; <Link to="/Windturbine">Windturbine</Link></p>
				{contents}
               
            </div>
        );
	}

	async getWindturbineData() {
		const response = await fetch('windturbine');
		const data = await response.json();
		this.setState({ windturbineData: data, loading: false });
	}
}
