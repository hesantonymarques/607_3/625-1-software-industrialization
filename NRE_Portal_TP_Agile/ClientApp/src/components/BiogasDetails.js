import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import CanvasJSReact from '../lib/canvasjs.react';
var CanvasJSChart = CanvasJSReact.CanvasJSChart;

export class BiogasDetails extends Component {
    static displayName = BiogasDetails.name;

    constructor(props) {
		super(props);
		this.state = { biogasData: [], loading: true };
	}

	componentDidMount() {
		this.getBioGasData();
	}

	static renderChart(data) {
		var data1 = [];

		const options = {
			animationEnabled: true,
			title: {
				text: "Development of renewable biogas energies since 1990 (KWh)"
			},
			axisX: {
				title: "Year",

            },
			
			axisY: {
				title: "Renewable Biogas in KWh",
				suffix: " KWh"
			},
			legend: {
				cursor: "pointer",
				fontSize: 16,
			},
			toolTip: {
				shared: true
			},
			data: [
			{
					name: "Renewable electricity production(excluding hydropower)",
				type: "spline",
				yValueFormatString: "#0.## KWh",
				showInLegend: true,
					dataPoints: data1,
				},
				
			]
		}

		for (var i = 0; i <= data.length; i++) {
			try {
				data1.push({ x: data[i].year, y: data[i].measure * 1000 });
			} catch {
				//Data will be generated automatically
            }
		}

		options.data[0].dataPoints = data1;

        return <CanvasJSChart options={options} />;

	}

	render() {
		let contents = this.state.loading
			? <p><em>Loading...</em></p>
			: BiogasDetails.renderChart(this.state.biogasData);
      
        return (
            <div>
                <h1>Biogas</h1>

				<p><Link to="/">Home</Link> &gt; <Link to="/NER">NER</Link> &gt; <Link to="/Biogas">Biogas</Link></p>
				{contents}
               
            </div>
        );
	}

	async getBioGasData() {
		const response = await fetch('biogas');
		const data = await response.json();
		this.setState({ biogasData: data, loading: false });
	
	}
}
