import React, { Component } from 'react';

export class Home extends Component {
  static displayName = Home.name;

	render() {
        return (
          <div>
				<h1>Portal of the new renewable energy in Valais</h1>
          </div>
        );
  }
}
